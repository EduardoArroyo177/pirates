﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class InGameCurrencyStoreController : MonoBehaviour
{
    [SerializeField]
    private GameObject notEnoughCoinsView;
    [Header("Prices")]
    [SerializeField]
    private int map1Cost;
    [SerializeField]
    private int map3Cost;
    [SerializeField]
    private int map10Cost;
    [Header("Scripts")]
    [SerializeField]
    private InGameCurrencyStoreView inGameCurrencyStoreView;
    [SerializeField]
    private MapView mapView;
    [Header("Ads")]
    [SerializeField]
    private AdsManager adsManager;
    [SerializeField]
    private VoidEvent GiveRewardEvent;

    private PlayerStats playerData;

    private void Start()
    {
        GiveRewardEvent.Register(GiveRewardEventCallback);
        LoadCoinsData();
        inGameCurrencyStoreView.SetPrices(map1Cost, map3Cost, map10Cost);
    }

    private void LoadCoinsData()
    {
        playerData = SaveSystem.instance.GetPlayerData();
        inGameCurrencyStoreView.SetCoins(playerData.totalCoins);
    }

    public void BuyTutorialMap()
    {
        Debug.Log("Can buy!");
    }

    public void Buy1Map()
    {
        if (playerData.totalCoins >= map1Cost)
        {
            Debug.Log("Can buy!");
            SaveSystem.instance.SaveMaps(1);
            SaveSystem.instance.SubstractCoins(map1Cost);
            LoadCoinsData();
        }
        else
        {
            notEnoughCoinsView.SetActive(true);
        }
    }

    public void Buy3Maps()
    {
        if (playerData.totalCoins >= map3Cost)
        {
            Debug.Log("Can buy!");
            SaveSystem.instance.SaveMaps(3);
            SaveSystem.instance.SubstractCoins(map3Cost);
            LoadCoinsData();
        }
        else
        {
            notEnoughCoinsView.SetActive(true);
        }
    }

    public void Buy10Maps()
    {
        if (playerData.totalCoins >= map10Cost)
        {
            Debug.Log("Can buy!");
            SaveSystem.instance.SaveMaps(10);
            SaveSystem.instance.SubstractCoins(map10Cost);
            LoadCoinsData();
        }
        else
        {
            notEnoughCoinsView.SetActive(true);
        }
    }

    public void GetFreeCoins()
    {
        adsManager.ShowRewardedAd();
    }

    private void GiveRewardEventCallback()
    {
        SaveSystem.instance.SaveCoins(150);
        LoadCoinsData();
    }

    public void CloseStore()
    {
        mapView.ShowPlayerStats(playerData);
    }

    private void OnDestroy()
    {
        GiveRewardEvent.Unregister(GiveRewardEventCallback);
    }
}
