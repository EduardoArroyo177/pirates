﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UDP;

public class UDPPurchaseManager : MonoBehaviour
{
    public List<string> productIds;
    public string productId;
    public string developerPayload;
    PurchaseListener purchaseListener;

    // Start is called before the first frame update
    public void GetStore()
    {
        purchaseListener = new PurchaseListener();
        StoreService.QueryInventory(productIds, purchaseListener);
        //StoreService.QueryInventory(purchaseListener);
    }

    public void PurchaseProduct()
    {
        StoreService.Purchase(productId, developerPayload, purchaseListener);
    }
}

public class PurchaseListener : IPurchaseListener
{
    public void OnPurchase(PurchaseInfo purchaseInfo)
    {
        Debug.Log("THE PURCHASE WAS A SUCCESSSSS");
        // The purchase has succeeded.
        // If the purchased product is consumable, you should consume it here.
        // Otherwise, deliver the product.
    }

    public void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo)
    {
        Debug.Log("Purchase Failed: " + message);
    }

    public void OnPurchaseRepeated(string productCode)
    {
        // Some stores don't support queryInventory.
        Debug.Log("PURCHASE ALREADY PURCHASED");
    }

    public void OnPurchaseConsume(PurchaseInfo purchaseInfo)
    {
        // The consumption succeeded.
        // You should deliver the product here.     
        Debug.Log("PURchAASE CONSUMED");
    }

    public void OnPurchaseConsumeFailed(string message, PurchaseInfo purchaseInfo)
    {
        // The consumption failed.
        Debug.Log("PURchAASE CONSUMED FAAAAILED");
    }

    public void OnQueryInventory(Inventory inventory)
    {
        // Querying inventory succeeded.
        Debug.Log($"INVENTORY QUERIED {inventory.GetProductDictionary().Count}");
    }

    public void OnQueryInventoryFailed(string message)
    {
        // Querying inventory failed.
        Debug.Log($"INVENTORY QUERIED FAILED!!!");
    }
}
