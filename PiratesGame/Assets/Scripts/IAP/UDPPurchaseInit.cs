﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UDP;

public class UDPPurchaseInit : MonoBehaviour
{
    private InitListener listener;

    void Start()
    {
        listener = new InitListener();
        StoreService.Initialize(listener);
    }
}


public class InitListener : IInitListener
{
    public void OnInitialized(UserInfo userInfo)
    {
        Debug.Log("Initialization succeeded");
        // You can call the QueryInventory method here
        // to check whether there are purchases that haven’t be consumed.       
    }

    public void OnInitializeFailed(string message)
    {
        Debug.Log("Initialization failed: " + message);
    }
}

