﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InGameCurrencyStoreView : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField]
    private InGameCurrencyStoreController inGameCurrencyStoreController;
    [Header("UI references")]
    [SerializeField]
    private TextMeshProUGUI currentCoins;
    [SerializeField]
    private TextMeshProUGUI oneMapTxt;
    [SerializeField]
    private TextMeshProUGUI threeMapsTxt;
    [SerializeField]
    private TextMeshProUGUI tenMapsTxt;

    public void SetCoins(int _coins)
    {
        currentCoins.text = $"x {_coins}";
    }
    

    public void SetPrices(int _oneMap, int _threeMaps, int _tenMaps)
    {
        oneMapTxt.text = $"{_oneMap} \n Coins";
        threeMapsTxt.text = $"{_threeMaps} \n Coins";
        tenMapsTxt.text = $"{_tenMaps} \n Coins";
    }
}
