﻿using HmsPlugin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HuaweiMobileServices.Ads;
using UnityAtoms.BaseAtoms;

public class AdsManager : MonoBehaviour
{
    public string REWARD_AD_ID = "testx9dtjwj8hp";
    [Header("Events")]
    [SerializeField]
    private VoidEvent GiveRewardEvent;

    //private const string INTERSTITIAL_AD_ID = "testb4znbuh3n2";

    private RewardAdManager rewardAdManager;
    //private InterstitalAdManager interstitialAdManager;

    // Start is called before the first frame update
    void Start()
    {
        InitRewardedAds();
    }

    private void InitRewardedAds()
    {
        rewardAdManager = GetComponent<RewardAdManager>();
        rewardAdManager.AdId = REWARD_AD_ID;
        rewardAdManager.OnRewarded = OnRewarded;
        rewardAdManager.LoadNextRewardedAd();
    }

    public void ShowRewardedAd()
    {
        Debug.Log("[HMS] AdsDemoManager ShowRewardedAd");
        rewardAdManager.ShowRewardedAd();
    }

    public void OnRewarded(Reward reward)
    {
        Debug.Log("[HMS] AdsDemoManager rewarded!");
        GiveRewardEvent.Raise();
        rewardAdManager.LoadNextRewardedAd();
    }
}
