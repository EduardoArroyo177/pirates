﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    public static SaveSystem instance;
    private const string PLAYER_DATA = "PlayerData";

    [SerializeField]
    private PlayerStats playerStats;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            LoadSavedData();
        }
        else
        { 
            Destroy(gameObject);
        }
    }

    private void LoadSavedData()
    {
        if (!PlayerPrefs.GetString(PLAYER_DATA).Equals(""))
        {
            playerStats = JsonUtility.FromJson<PlayerStats>(PlayerPrefs.GetString(PLAYER_DATA));
        }
    }

    #region Coins
    public void SaveCoins(int _coins)
    {
        playerStats.totalCoins += _coins;
        SavePlayerData(JsonUtility.ToJson(playerStats));
    }

    public void SubstractCoins(int _coins)
    {
        playerStats.totalCoins -= _coins;
        SavePlayerData(JsonUtility.ToJson(playerStats));
    }
    #endregion

    #region Maps
    public void SaveMaps(int _maps)
    {
        playerStats.maps += _maps;
        SavePlayerData(JsonUtility.ToJson(playerStats));
    }

    public void SubstractMaps(int _maps)
    {
        playerStats.maps -= _maps;
        SavePlayerData(JsonUtility.ToJson(playerStats));
    }
    #endregion

    #region Islands
    public void UnlockIsland(int _island)
    {
        switch (_island)
        {
            case 1: // Island 2
                playerStats.island2Unlocked = true;
                break;
            case 2:
                playerStats.island3Unlocked = true;
                break;
            case 3:
                playerStats.island4Unlocked = true;
                break;
            case 4:
                playerStats.island5Unlocked = true;
                break;
            case 5:
                playerStats.island6Unlocked = true;
                break;
        }
        SavePlayerData(JsonUtility.ToJson(playerStats));
    }
    #endregion

    #region Diamonds
    public void SaveDiamonds(int _blueDiamonds, int _greenDiamonds, int _redDiamonds)
    {
        playerStats.blueDiamonds = _blueDiamonds;
        playerStats.greenDiamonds = _greenDiamonds;
        playerStats.redDiamonds = _redDiamonds;
        SavePlayerData(JsonUtility.ToJson(playerStats));
    }
    #endregion

    public void SavePlayerData(string _playerData)
    {
        PlayerPrefs.SetString(PLAYER_DATA, _playerData);
    }

    public string GetPlayerDataString()
    {
        return PlayerPrefs.GetString(PLAYER_DATA);
    }

    public PlayerStats GetPlayerData()
    {
        return playerStats;
    }
}

[System.Serializable]
public class PlayerStats
{
    // Currency stuff
    public int totalCoins;
    public int bronzeChests;
    public int silverChests;
    public int goldenChests;
    public int platinumChests;
    public int blueDiamonds;
    public int greenDiamonds;
    public int redDiamonds;
    public int maps;

    // Unlocked islands
    public bool island2Unlocked;
    public bool island3Unlocked;
    public bool island4Unlocked;
    public bool island5Unlocked;
    public bool island6Unlocked;
}