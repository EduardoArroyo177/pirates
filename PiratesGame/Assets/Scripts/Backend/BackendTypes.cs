﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Characters
{ 
    clownPirate,
    star,
    crabby,
    tooth
}

public class BackendTypes
{
    
}

public class PlayerData
{
    // UI
    public int playerLevel;
    public Characters lastUsedCharacter;
    public Sprite lastUsedCharacterSprite;

    // General data
    public int playerExperience;
    public int playerCoins;
    public int playerMaps;
}
