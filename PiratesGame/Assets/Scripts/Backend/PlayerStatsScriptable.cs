﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Pirates/PlayerStats", order = 1)]
public class PlayerStatsScriptable : ScriptableObject
{
    [SerializeField]
    private PlayerStats playerStats;
    //[SerializeField]
    //private int totalCoins;
    //[SerializeField]
    //private int bronzeChests;
    //[SerializeField]
    //private int silverChests;
    //[SerializeField]
    //private int goldenChests;
    //[SerializeField]
    //private int platinumChests;
    //[SerializeField]
    //private int blueDiamonds;
    //[SerializeField]
    //private int greenDiamonds;
    //[SerializeField]
    //private int redDiamonds;

    //public int TotalCoins { get => totalCoins; set => totalCoins = value; }
    //public int BronzeChests { get => bronzeChests; set => bronzeChests = value; }
    //public int SilverChests { get => silverChests; set => silverChests = value; }
    //public int GoldenChests { get => goldenChests; set => goldenChests = value; }
    //public int PlatinumChests { get => platinumChests; set => platinumChests = value; }
    //public int BlueDiamonds { get => blueDiamonds; set => blueDiamonds = value; }
    //public int GreenDiamonds { get => greenDiamonds; set => greenDiamonds = value; }
    //public int RedDiamonds { get => redDiamonds; set => redDiamonds = value; }
}
