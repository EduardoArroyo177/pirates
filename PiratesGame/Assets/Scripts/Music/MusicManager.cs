﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    
    [SerializeField]
    private AudioClip[] musicTracks;

    [Header("Events")]
    [SerializeField]
    private IntEvent musicVolumeEvent;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        musicVolumeEvent.Register(SetMusicVolume);
        LoadSong();
    }

    private void Start()
    {
        SetCurrentMusicVolume();
    }

    private void LoadSong()
    {
        audioSource.clip = musicTracks[Random.Range(0, musicTracks.Length)];
        audioSource.Play();
    }

    private void SetCurrentMusicVolume()
    {
        audioSource.volume = (float)SoundSettingsController.instance.MusicVolume / 10;
    }

    private void SetMusicVolume(int _volume)
    {
        audioSource.volume = (float)_volume / 10;
    }

    private void OnDestroy()
    {
        musicVolumeEvent.Unregister(SetMusicVolume);
    }
}
