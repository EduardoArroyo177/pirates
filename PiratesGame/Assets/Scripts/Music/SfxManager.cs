﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class SfxManager : MonoBehaviour
{
    [SerializeField]
    private AudioClip soundEffect;

    [Header("Events")]
    [SerializeField]
    private IntEvent sfxVolumeEvent;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        sfxVolumeEvent.Register(SetSfxVolume);
    }

    private void Start()
    {
        SetCurrentSfxVolume();
    }

    public void PlaySound()
    {
        audioSource.clip = soundEffect;
        audioSource.Play();
    }

    private void SetCurrentSfxVolume()
    {
        audioSource.volume = (float)SoundSettingsController.instance.SfxVolume / 10;
    }

    private void SetSfxVolume(int _volume)
    {
        audioSource.volume = (float)_volume / 10;
    }

    private void OnDestroy()
    {
        sfxVolumeEvent.Unregister(SetSfxVolume);
    }
}
