﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct UsedCharacter
{
    public Characters character;
    public Sprite characterSprite;
}

public class MainMenuModel : MonoBehaviour
{
    private List<UsedCharacter> availableCharacters;

    public List<UsedCharacter> AvailableCharacters { get => availableCharacters; set => availableCharacters = value; }

    private void Awake()
    {
        //Debug.Log($"INVENTORY {Inventory.instance.AvailableCharacters}");
        //availableCharacters = Inventory.instance.AvailableCharacters;
    }
}
