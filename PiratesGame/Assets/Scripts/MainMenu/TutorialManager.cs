﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [Header("Tutorial Part 1")]
    [SerializeField]
    private GameObject tutorialPanel1;
    [SerializeField]
    private GameObject[] tutorial1TextList;

    [Header("Tutorial Part 2")]
    [SerializeField]
    private GameObject tutorialPanel2;
    [SerializeField]
    private GameObject[] tutorial2TextList;

    [Header("Tutorial Part 3")]
    [SerializeField]
    private GameObject tutorialPanel3;
    [SerializeField]
    private GameObject[] tutorial3TextList;

    [Header("Tutorial Part 4")]
    [SerializeField]
    private GameObject tutorialPanel4;
    [SerializeField]
    private GameObject[] tutorial4TextList;

    [Header("Tutorial Part 5")]
    [SerializeField]
    private GameObject tutorialPanel5;
    [SerializeField]
    private GameObject[] tutorial5TextList;

    [Header("Tutorial Part 6")]
    [SerializeField]
    private GameObject tutorialPanel6;
    [SerializeField]
    private GameObject[] tutorial6TextList;

    [Header("Tutorial Part 7")]
    [SerializeField]
    private GameObject tutorialPanel7;
    [SerializeField]
    private GameObject[] tutorial7TextList;

    private int tutorial1CurrentStep;
    private bool tutorial1Finished;

    public void ShowTutorial()
    { 
    
    }

    public void TutorialPart1Next()
    {
        tutorial1TextList[tutorial1CurrentStep].SetActive(false);
        tutorial1CurrentStep++;
        if (tutorial1CurrentStep < tutorial1TextList.Length)
        {
            tutorial1TextList[tutorial1CurrentStep].SetActive(true);
        }
    }

}
