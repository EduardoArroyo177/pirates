﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadController : MonoBehaviour
{
    [SerializeField]
    private GameObject loadPanel;
    [SerializeField]
    private Image fillbarImage;

    public void LoadIslandScene(int _islandIndex)
    {
        loadPanel.SetActive(true);
        StartCoroutine(LoadIslandAsync($"Island{_islandIndex}"));
    }

    IEnumerator LoadIslandAsync(string _islandSceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_islandSceneName);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
            fillbarImage.fillAmount = asyncLoad.progress;
        }
    }

    public void LoadMainMenu()
    {
        StartCoroutine("LoadMainMenuAsync");
    }

    IEnumerator LoadMainMenuAsync()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainMenu");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
