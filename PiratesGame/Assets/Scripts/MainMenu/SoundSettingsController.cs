﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class SoundSettingsController : MonoBehaviour
{
    public static SoundSettingsController instance;

    [Header("Events")]
    [SerializeField]
    private IntEvent musicVolumeEvent;
    [SerializeField]
    private IntEvent sfxVolumeEvent;

    private int musicVolume;
    private int sfxVolume;

    private int volumeLimit = 10;

    public int MusicVolume { get => musicVolume; set => musicVolume = value; }
    public int SfxVolume { get => sfxVolume; set => sfxVolume = value; }
    public int VolumeLimit { get => volumeLimit; }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            LoadVolumeSettings();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void LoadVolumeSettings()
    {
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            MusicVolume = PlayerPrefs.GetInt("MusicVolume");
            SfxVolume = PlayerPrefs.GetInt("SfxVolume");
        }
        else
        {
            MusicVolume = 7;
            sfxVolume = volumeLimit;
        }
    }

    public void MusicVolumeUp()
    {
        if (MusicVolume < VolumeLimit)
        {
            MusicVolume++;
        }
        musicVolumeEvent.Raise(MusicVolume);
    }

    public void MusicVolumeDown()
    {
        if (MusicVolume > 0)
        {
            MusicVolume--;
        }
        musicVolumeEvent.Raise(MusicVolume);
    }

    public void SfxVolumeUp()
    {
        if (SfxVolume < VolumeLimit)
        {
            SfxVolume++;
        }
        sfxVolumeEvent.Raise(SfxVolume);
    }

    public void SfxVolumeDown()
    {
        if (SfxVolume > 0)
        {
            SfxVolume--;
        }
        sfxVolumeEvent.Raise(SfxVolume);
    }

    public void SaveCurrentVolume()
    {
        PlayerPrefs.SetInt("MusicVolume", MusicVolume);
        PlayerPrefs.SetInt("SfxVolume", SfxVolume);
    }
}
