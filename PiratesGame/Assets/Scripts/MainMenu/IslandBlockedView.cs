﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public struct IslandCost
{
    public int blueDiamonds;
    public int greenDiamonds;
    public int redDiamonds;
}

public class IslandBlockedView : MonoBehaviour
{

    [SerializeField]
    private IslandCost[] islandCost;

    [Header("UI References")]
    [SerializeField]
    private TextMeshProUGUI blueDiamondsTxt;
    [SerializeField]
    private TextMeshProUGUI greenDiamondsTxt;
    [SerializeField]
    private TextMeshProUGUI redDiamondsTxt;

    public IslandCost[] IslandCost { get => islandCost; set => islandCost = value; }

    public void ShowIslandCost(int _selectedIsland)
    {
        blueDiamondsTxt.text = IslandCost[_selectedIsland].blueDiamonds.ToString();
        greenDiamondsTxt.text = IslandCost[_selectedIsland].greenDiamonds.ToString();
        redDiamondsTxt.text = IslandCost[_selectedIsland].redDiamonds.ToString();
        gameObject.SetActive(true);
    }
}
