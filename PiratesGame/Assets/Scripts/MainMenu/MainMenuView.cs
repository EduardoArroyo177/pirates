﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuView : MonoBehaviour
{
    [Header("UI References")]
    [SerializeField]
    private Image lastUsedCharacterImg;
    [SerializeField]
    private Image lastUsedCharacterIcon;
    [SerializeField]
    private TextMeshProUGUI playerLevel;

    public void SetPlayerData(string playerDataJson)
    {
        PlayerData playerData = JsonUtility.FromJson<PlayerData>(playerDataJson);
        playerLevel.text = $"Lvl { playerData.playerLevel.ToString()}";
        lastUsedCharacterImg.sprite = playerData.lastUsedCharacterSprite;
        lastUsedCharacterIcon.sprite = playerData.lastUsedCharacterSprite;
    }
}
