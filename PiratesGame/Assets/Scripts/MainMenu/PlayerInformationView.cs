﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerInformationView : MonoBehaviour
{
    [Header("UI References")]
    [SerializeField]
    private TextMeshProUGUI coinsTxt;
    [SerializeField]
    private TextMeshProUGUI mapsTxt;
    [SerializeField]
    private TextMeshProUGUI blueDiamondsTxt;
    [SerializeField]
    private TextMeshProUGUI greenDiamondsTxt;
    [SerializeField]
    private TextMeshProUGUI redDiamondsTxt;

    private PlayerStats playerData;

    private void OnEnable()
    {
        LoadData();
    }

    private void LoadData()
    {
        playerData = SaveSystem.instance.GetPlayerData();
        coinsTxt.text = playerData.totalCoins.ToString();
        mapsTxt.text = playerData.maps.ToString();
        blueDiamondsTxt.text = playerData.blueDiamonds.ToString();
        greenDiamondsTxt.text = playerData.greenDiamonds.ToString();
        redDiamondsTxt.text = playerData.redDiamonds.ToString();
    }
}
