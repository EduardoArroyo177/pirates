﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OptionsView : MonoBehaviour
{
    [Header("UI References")]
    [SerializeField]
    private TextMeshProUGUI musicVolumeTxt;
    [SerializeField]
    private TextMeshProUGUI sfxVolumeTxt;
    [SerializeField]
    private TextMeshProUGUI gameVersionTxt;

    private void OnEnable()
    {
        ShowCurrentVolume();
        ShowGameVersion();
    }

    public void ShowCurrentVolume()
    {
        musicVolumeTxt.text = SoundSettingsController.instance.MusicVolume.ToString();
        sfxVolumeTxt.text = SoundSettingsController.instance.SfxVolume.ToString();
    }

    public void MusicVolumeUp()
    {
        SoundSettingsController.instance.MusicVolumeUp();
        musicVolumeTxt.text = SoundSettingsController.instance.MusicVolume.ToString();
    }

    public void MusicVolumeDown()
    {
        SoundSettingsController.instance.MusicVolumeDown();
        musicVolumeTxt.text = SoundSettingsController.instance.MusicVolume.ToString();
    }

    public void SfxVolumeUp()
    {
        SoundSettingsController.instance.SfxVolumeUp();
        sfxVolumeTxt.text = SoundSettingsController.instance.SfxVolume.ToString();
    }
    

    public void SfxVolumeDown()
    {
        SoundSettingsController.instance.SfxVolumeDown();
        sfxVolumeTxt.text = SoundSettingsController.instance.SfxVolume.ToString();
    }

    public void CloseOptionsView()
    {
        SoundSettingsController.instance.SaveCurrentVolume();
    }

    private void ShowGameVersion()
    {
        gameVersionTxt.text = Application.version;
    }
}
