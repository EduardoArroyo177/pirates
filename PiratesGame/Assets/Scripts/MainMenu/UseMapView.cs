﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseMapView : MonoBehaviour
{
    [SerializeField]
    private LoadController loadController;
    [SerializeField]
    private int selectedIsland;

    public int SelectedIsland { get => selectedIsland; set => selectedIsland = value; }

    public void UseMap()
    {
        ChestSystem.instance.HasAdventureMap = true;
        SaveSystem.instance.SubstractMaps(1);
        loadController.LoadIslandScene(selectedIsland);
    }

    public void DontUseMap()
    {
        ChestSystem.instance.HasAdventureMap = false;
        loadController.LoadIslandScene(selectedIsland);
    }
}
