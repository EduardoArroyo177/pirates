﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NotEnoughCoinsView : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI currentCoinsTxt;

    private PlayerStats playerData;

    private void OnEnable()
    {
        LoadCurrentCoins();
    }

    private void LoadCurrentCoins()
    {
        playerData = SaveSystem.instance.GetPlayerData();
        currentCoinsTxt.text = $"x {playerData.totalCoins}";
    }
}
