﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityAtoms.BaseAtoms;
using UnityEngine;


[CreateAssetMenu(fileName = "MinigameBase", menuName = "Pirate Data/Minigame base", order = 1)]

public class BaseMinigameData : ScriptableObject
{
    [SerializeField]
    private string minigameName;
    [SerializeField]
    private string minigameInstructions;
    [SerializeField]
    private GameObject minigamePrefab;

    [Header("Game rewards")]
    [SerializeField]
    private int coinsToWin;
    [SerializeField]
    private int[] coinsToWinPerRound;

    [Header("Other variables")]
    [SerializeField]
    private bool timerFinishedWins;

    [Header("Events")]
    [SerializeField]
    private VoidEvent StartMinigameEvent;
    [SerializeField]
    private VoidEvent StartGameEvent;
    [SerializeField]
    private BoolEvent StopMinigameEvent;

    public string MinigameName { get => minigameName; set => minigameName = value; }
    public string MinigameInstructions { get => minigameInstructions; set => minigameInstructions = value; }
    public GameObject MinigamePrefab { get => minigamePrefab; set => minigamePrefab = value; }
    public int CoinsToWin { get => coinsToWin; set => coinsToWin = value; }
    public bool TimerFinishedWins { get => timerFinishedWins; set => timerFinishedWins = value; }

    private GameObject minigame;
    private int currentRound;

    public void Initialize()
    {
        StartMinigameEvent.Register(StartMinigame);
    }

    public void SetCurrentRound(int _currentRound)
    {
        currentRound = _currentRound;
        coinsToWin = coinsToWinPerRound[currentRound];
    }

    public void StartMinigame()
    {
        // Start prefab
        minigame = Instantiate(minigamePrefab) as GameObject;
        // Set minigame round
        MinigameRoundValuesController roundValuesController = minigame.GetComponent<MinigameRoundValuesController>();
        if (roundValuesController)
        { 
            roundValuesController.SetRoundValue(currentRound);
        }
        minigame.SetActive(true);
        // Start timer
        StartGameEvent.Raise();
    }

    public void StopMinigame(bool _gameWon)
    {
        StartMinigameEvent.Unregister(StartMinigame);
        StopMinigameEvent.Unregister(StopMinigame);
        Destroy(minigame);
    }

    private void OnDestroy()
    {
        StartMinigameEvent.UnregisterAll();
        StopMinigameEvent.UnregisterAll();
    }
}
