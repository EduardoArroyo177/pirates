﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameplayBase", menuName = "Pirate Data/Gameplay base", order = 1)]
public class BaseGameplayData : ScriptableObject
{
    [SerializeField]
    private float[] roundsPlayTime;
    [SerializeField]
    private int totalRounds;

    public float[] RoundsPlayTime { get => roundsPlayTime; set => roundsPlayTime = value; }
    public int TotalRounds { get => totalRounds; set => totalRounds = value; }
}
