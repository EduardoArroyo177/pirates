﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigamePool : MonoBehaviour
{
    [SerializeField]
    private List<BaseMinigameData> minigameList;

    public BaseMinigameData GetMinigame()
    {
        int randomGame = Random.Range(0, minigameList.Count);
        return minigameList[randomGame];
    }
}
