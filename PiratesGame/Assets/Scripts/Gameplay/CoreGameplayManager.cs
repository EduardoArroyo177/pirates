﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CoreGameplayManager : MonoBehaviour
{
    [Header("Script references")]
    

    [Header("Variables")]
    [SerializeField]
    private int minigamesFinishedToNewRound;
    [SerializeField]
    private int totalRounds;

    // Scripts
    public MinigamePool minigamePool { get; set; }
    public MinigameManager minigameManager { get; set; }
    public GameplayController gameplayController { get; set; }
    public MissionFinishedController missionFinishedController { get; set; }

    // Events
    public VoidEvent StartMinigameEvent { get; set; }
    // Properties
    public int CurrentRound { get => currentRound; set => currentRound = value; }
    public bool ShowFasterView { get => showFasterView; set => showFasterView = value; }

    // TODO: Make these private at the end
    public BaseMinigameData currentMinigame;
    public int currentRound;
    public int totalMinigamesStarted;
    private bool showFasterView;

    public void Initialize()
    {
        GetMinigameFromPool();
        SetGeneralData();
        SetMinigameData();
        SetSpecificRoundMinigameData();
        StartMinigame();
    }

    public void GetMinigameFromPool()
    {
        BaseMinigameData newMinigame = minigamePool.GetMinigame();
        if (newMinigame.Equals(currentMinigame))
        {
            GetMinigameFromPool();
        }
        else
        {
            currentMinigame = newMinigame;
            currentMinigame.Initialize();
        }
    }

    public void SetGeneralData()
    {
        minigameManager.Initialize();
    }

    public void SetMinigameData()
    {
        StringPair gameData = new StringPair();
        gameData.Item1 = currentMinigame.MinigameName;
        gameData.Item2 = currentMinigame.MinigameInstructions;
        gameplayController.SetMinigameData(gameData);
        gameplayController.SetTimerFinishedBehaviorData(currentMinigame.TimerFinishedWins);
    }

    public void SetSpecificRoundMinigameData()
    {
        currentMinigame.SetCurrentRound(currentRound);
    }

    public void StartMinigame()
    {
        totalMinigamesStarted++;
        StartMinigameEvent.Raise();
    }

    public void PrepareNextMinigame()
    {
        currentMinigame.StopMinigame(false);
        GetMinigameFromPool();
        SetGeneralData();
        SetMinigameData();
        if (totalMinigamesStarted >= minigamesFinishedToNewRound)
        {
            if (currentRound >= totalRounds)
            {
                Debug.Log("WON THE INTERNET!");
                ChestSystem.instance.CurrentEarnedChest = 4;
                missionFinishedController.GameFinished();
            }
            else
            {
                ShowFasterView = true;
                currentRound++;
                totalMinigamesStarted = 0;
                SetSpecificRoundMinigameData();
            }
        }
    }

    public void StartNextMinigame()
    {
        StartMinigame();
    }

    public void GameOver()
    {
        currentMinigame.StopMinigame(false);
    }
}
