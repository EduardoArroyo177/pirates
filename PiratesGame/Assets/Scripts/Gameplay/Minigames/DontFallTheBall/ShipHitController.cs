﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipHitController : MonoBehaviour
{
    private Animator shipAnimator;

    private void Awake()
    {
        shipAnimator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("CannonBall"))
        {
            shipAnimator.SetTrigger("Dead");
        }
    }
}
