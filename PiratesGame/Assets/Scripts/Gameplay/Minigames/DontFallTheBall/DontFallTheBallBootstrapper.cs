﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class DontFallTheBallBootstrapper : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField]
    private MinigameRoundValuesController roundValuesController;
    [SerializeField]
    private CannonBallController cannonBallController;

    [Header("Events")]
    [SerializeField]
    private IntEvent currentRoundEvent;

    private void Start()
    {
        cannonBallController.CurrentRoundEvent = currentRoundEvent;
        cannonBallController.Initialze();

        roundValuesController.CurrentRoundEvent = currentRoundEvent;
        roundValuesController.Initialize();
    }

    private void OnDestroy()
    {
        cannonBallController.UnregisterEvents();
    }
}
