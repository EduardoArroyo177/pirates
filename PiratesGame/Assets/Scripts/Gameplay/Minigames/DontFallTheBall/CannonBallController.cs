﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CannonBallController : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private float[] movementSpeedPerRound;
    [SerializeField]
    private float jumpSpeed;

    [Header("Events")]
    [SerializeField]
    private VoidEvent tapEvent;

    public IntEvent CurrentRoundEvent { get; set; }

    private Rigidbody2D characterRigidbody2D;
    private bool canJump;
    public bool isGrounded;

    void Awake()
    {
        characterRigidbody2D = GetComponent<Rigidbody2D>();
        tapEvent.Register(TapJumptEventCallback);
    }

    public void Initialze()
    {
        CurrentRoundEvent.Register(CurrentRoundEventCallback);
    }

    private void CurrentRoundEventCallback(int _currentRound)
    {
        movementSpeed = movementSpeedPerRound[_currentRound];
    }

    void Update()
    {
        transform.Translate(Vector2.right * movementSpeed * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (canJump)
        {
            canJump = false;
            characterRigidbody2D.AddForce(Vector2.up * jumpSpeed);
        }
    }

    private void TapJumptEventCallback()
    {
        canJump = true;
    }

    public void UnregisterEvents()
    {
        tapEvent.Unregister(TapJumptEventCallback);
        CurrentRoundEvent.Unregister(CurrentRoundEventCallback);
    }
}
