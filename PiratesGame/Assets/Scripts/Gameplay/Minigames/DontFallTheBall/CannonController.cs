﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour
{

    [SerializeField]
    private GameObject cannonEffect;
    [SerializeField]
    private GameObject cannonBall;

    public void PlayCannonEffectAnimation()
    {
        
    }

    public void ShootBall()
    {
        cannonEffect.SetActive(true);
        cannonBall.SetActive(true);
    }
}
