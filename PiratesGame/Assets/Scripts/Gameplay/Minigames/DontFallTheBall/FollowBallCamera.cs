﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBallCamera : MonoBehaviour
{
    [SerializeField]
    private GameObject cannonBall;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(cannonBall.transform.position.x, transform.position.y, transform.position.z);
    }
}
