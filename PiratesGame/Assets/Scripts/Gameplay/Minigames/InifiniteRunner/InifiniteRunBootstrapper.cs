﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class InifiniteRunBootstrapper : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField]
    private MinigameRoundValuesController roundValuesController;
    [SerializeField]
    private CharacterMovement_Infinite characterMovement;

    [Header("Events")]
    [SerializeField]
    private IntEvent currentRoundEvent;

    private void Start()
    {
        characterMovement.CurrentRoundEvent = currentRoundEvent;
        characterMovement.Initialze();

        roundValuesController.CurrentRoundEvent = currentRoundEvent;
        roundValuesController.Initialize();
    }

    private void OnDestroy()
    {
        characterMovement.UnregisterEvents();
    }
}
