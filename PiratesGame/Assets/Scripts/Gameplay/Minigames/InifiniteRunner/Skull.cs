﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skull : MonoBehaviour
{
    [Header("Animation variables")]
    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private float upperLimit;
    [SerializeField]
    private float lowerLimit;

    private bool isGoingUp;

    private Vector3 originalPosition;

    void Awake()
    {
        originalPosition = transform.position;
    }

    private void Update()
    {
        if (isGoingUp)
        {
            transform.Translate(Vector3.up * movementSpeed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.down * movementSpeed * Time.deltaTime);
        }

        if (transform.position.y >= (originalPosition.y + upperLimit))
        {
            isGoingUp = false;
        }
        else if (transform.position.y <= (originalPosition.y - lowerLimit))
        {
            isGoingUp = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.SetActive(false);
    }
}
