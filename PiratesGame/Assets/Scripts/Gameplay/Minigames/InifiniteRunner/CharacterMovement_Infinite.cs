﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CharacterMovement_Infinite : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private float[] movementSpeedPerRound;
    [SerializeField]
    private float jumpSpeed;
    [SerializeField]
    private Vector3 raycastOffset;
    [SerializeField]
    private float raycastDistance;

    [Header("Events")]
    [SerializeField]
    private VoidEvent tapEvent;

    public IntEvent CurrentRoundEvent { get; set; }

    private Animator characterAnimator;
    private Rigidbody2D characterRigidbody2D;
    private AudioSource audioSource;
    private bool canJump;
    public bool isGrounded;
    private bool playedRunAnimation;
    private bool playedJumpAnimation;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        characterAnimator = GetComponent<Animator>();
        characterRigidbody2D = GetComponent<Rigidbody2D>();
        tapEvent.Register(TapJumptEventCallback);
    }

    public void Initialze()
    {
        CurrentRoundEvent.Register(CurrentRoundEventCallback);
    }

    private void CurrentRoundEventCallback(int _currentRound)
    {
        movementSpeed = movementSpeedPerRound[_currentRound];
    }

    void Update()
    {
        transform.Translate(Vector2.right * movementSpeed * Time.deltaTime);

        RaycastHit2D hit = Physics2D.Raycast(transform.position + raycastOffset, -Vector2.up, raycastDistance);
        Debug.DrawRay(transform.position + raycastOffset, -Vector2.up * raycastDistance, Color.red, 10.0f);

        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Ground"))
            {
                isGrounded = true;
            }
        }
        else
        {
            isGrounded = false;
            canJump = false;
        }

        if (isGrounded && !playedRunAnimation)
        {
            playedJumpAnimation = false;
            playedRunAnimation = true;
            characterAnimator.SetTrigger("Run");
        }

        if (!isGrounded && !playedJumpAnimation)
        {
            audioSource.Play();
            playedRunAnimation = false;
            playedJumpAnimation = true;
            characterAnimator.SetTrigger("Jump");
        }
    }

    private void FixedUpdate()
    {
        if (canJump && isGrounded)
        {
            canJump = false;
            characterRigidbody2D.AddForce(Vector2.up * jumpSpeed);
            characterAnimator.SetTrigger("Jump");
        }
    }

    private void TapJumptEventCallback()
    {
        canJump = true;
    }

    public void UnregisterEvents()
    {
        tapEvent.Unregister(TapJumptEventCallback);
        CurrentRoundEvent.Unregister(CurrentRoundEventCallback);
    }
}
