﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class MinigameRoundValuesController : MonoBehaviour
{
    public IntEvent CurrentRoundEvent { get; set; }
    public int CurrentRound { get => currentRound; set => currentRound = value; }

    public int currentRound;

    public void SetRoundValue(int _currentRound)
    {
        currentRound = _currentRound;
    }

    public void Initialize()
    {
        CurrentRoundEvent.Raise(currentRound);
    }
}
