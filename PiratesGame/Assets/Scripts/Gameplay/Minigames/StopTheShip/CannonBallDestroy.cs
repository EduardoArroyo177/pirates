﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CannonBallDestroy : MonoBehaviour
{
    [Header("Events")]
    [SerializeField]
    private VoidEvent cannonBallDestroyedEvent;

    private void OnDisable()
    {
        DestroyBall();
    }

    private void DestroyBall()
    {
        cannonBallDestroyedEvent.Raise();
        //gameObject.SetActive(false);
    }
}
