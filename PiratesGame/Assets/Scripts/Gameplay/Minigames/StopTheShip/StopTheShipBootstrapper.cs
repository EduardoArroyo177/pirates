﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class StopTheShipBootstrapper : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField]
    private MinigameRoundValuesController roundValuesController;
    [SerializeField]
    private ShipMovement shipMovement;
    [SerializeField]
    private CannonBallRoundController[] cannonBallRoundController;
    [SerializeField]
    private CannonBallFallingController[] cannonBallFallingController;

    [Header("Events")]
    [SerializeField]
    private IntEvent currentRoundEvent;

    private void Start()
    {
        shipMovement.CurrentRoundEvent = currentRoundEvent;
        shipMovement.Initialze();

        for (int i = 0; i < cannonBallRoundController.Length; i++)
        {
            cannonBallRoundController[i].CurrentRoundEvent = currentRoundEvent;
            cannonBallRoundController[i].Initialize();
        }

        for (int i = 0; i < cannonBallFallingController.Length; i++)
        {
            cannonBallFallingController[i].CurrentRoundEvent = currentRoundEvent;
            cannonBallFallingController[i].Initialze();
        }
       
        roundValuesController.CurrentRoundEvent = currentRoundEvent;
        roundValuesController.Initialize();
    }

    private void OnDestroy()
    {
        shipMovement.UnregisterEvents();

        for (int i = 0; i < cannonBallRoundController.Length; i++)
        {
            cannonBallRoundController[i].UnregisterEvents();
        }

        for (int i = 0; i < cannonBallFallingController.Length; i++)
        {
            cannonBallFallingController[i].UnregisterEvents();
        }
    }
}
