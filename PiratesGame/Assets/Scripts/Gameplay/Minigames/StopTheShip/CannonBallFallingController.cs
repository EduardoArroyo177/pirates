﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CannonBallFallingController : MonoBehaviour
{
    [Header("Marker data")]
    [SerializeField]
    private GameObject marker;
    [SerializeField]
    private float[] blinkingSpeed;
    [SerializeField]
    private int numberOfBlinks;

    [Header("Cannon ball data")]
    [SerializeField]
    private GameObject cannonBall;
    [SerializeField]
    private GameObject cannonBallMarker;

    [Header("Events")]
    [SerializeField]
    private VoidEvent cannonBallDestroyedEvent;

    public IntEvent CurrentRoundEvent { get; set; }

    private Vector3 cannonBallOriginalPosition;
    private float currentBlinkingSpeed;

    private void Awake()
    {
        cannonBallOriginalPosition = cannonBall.transform.position;
    }

    private void OnDisable()
    {
        cannonBallDestroyedEvent.Unregister(CannonBallDestroyedEventCallback);
    }

    public void Initialze()
    {
        CurrentRoundEvent.Register(CurrentRoundEventCallback);
    }

    private void CurrentRoundEventCallback(int _currentRound)
    {
        // Set values
        currentBlinkingSpeed = blinkingSpeed[_currentRound];
    }

    public void ShootBall()
    {
        cannonBall.transform.position = new Vector3(marker.transform.position.x, cannonBallOriginalPosition.y,
            cannonBallOriginalPosition.z);
        cannonBallDestroyedEvent.Register(CannonBallDestroyedEventCallback);
        StartCoroutine("MarkerBlink");
    }

    public void CannonBallDestroyedEventCallback()
    {
        StopCoroutine("MarkerBlink");
        gameObject.SetActive(false);
    }

    private IEnumerator MarkerBlink()
    {
        int blinks = 0;
        while (true)
        {
            yield return new WaitForSeconds(currentBlinkingSpeed);

            if (marker.activeInHierarchy)
            {
                marker.SetActive(false);
            }
            else
            {
                marker.SetActive(true);
            }

            blinks++;

            if (blinks >= numberOfBlinks)
            {
                marker.SetActive(true);
                cannonBall.SetActive(true);
                // Shoot ball
                break;
            }
        }
    }

    public void UnregisterEvents()
    {
        CurrentRoundEvent.Unregister(CurrentRoundEventCallback);
    }
}
