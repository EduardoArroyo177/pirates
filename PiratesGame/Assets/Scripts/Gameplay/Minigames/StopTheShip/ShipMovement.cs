﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class ShipMovement : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private float[] movementSpeedPerRound;

    [Header("Events")]
    [SerializeField]
    private VoidEvent holdTapEvent;
    [SerializeField]
    private VoidEvent releaseTapEvent;

    public IntEvent CurrentRoundEvent { get; set; }
    public bool CanMove { get => canMove; set => canMove = value; }

    private bool canMove;

    void Awake()
    {
        //movementSpeed = movementSpeedPerRound[0];
        //canMove = true;
        holdTapEvent.Register(StopShipEventCallback);
        releaseTapEvent.Register(ResumeShipEventCallback);
    }

    public void Initialze()
    {
        CurrentRoundEvent.Register(CurrentRoundEventCallback);
    }

    private void CurrentRoundEventCallback(int _currentRound)
    {
        // Set values 
        // Change the canmove to here so it starts after setting the values
        movementSpeed = movementSpeedPerRound[_currentRound];
        CanMove = true;
    }

    void Update()
    {
        if (CanMove)
        {
            transform.Translate(Vector2.right * movementSpeed * Time.deltaTime);
        }
    }

    private void StopShipEventCallback()
    {
        CanMove = false;
    }

    private void ResumeShipEventCallback()
    {
        CanMove = true;
    }

    public void UnregisterEvents()
    {
        holdTapEvent.Unregister(StopShipEventCallback);
        releaseTapEvent.Unregister(ResumeShipEventCallback);
        CurrentRoundEvent.Unregister(CurrentRoundEventCallback);
    }
}
