﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CannonBallExplosion : MonoBehaviour
{
    [SerializeField]
    private CannonBallFallingController cannonBallFallingController;
    [SerializeField]
    private bool isPlayer;
    [SerializeField]
    private BoolEvent stopMinigameEvent;

    private Animator currentAnimator;
    private Rigidbody2D currentRigidBody2D;
    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        currentAnimator = GetComponent<Animator>();
        currentRigidBody2D = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Activator") && !collision.CompareTag("CannonBall"))
        {
            audioSource.Play();
            if (isPlayer && collision.CompareTag("Obstacle"))
            {
                Debug.Log("Game over");
                stopMinigameEvent.Raise(false);
            }
            else if (isPlayer && collision.CompareTag("Enemy"))
            {
                Debug.Log("Won!");
                stopMinigameEvent.Raise(true);
            }
            currentRigidBody2D.velocity = Vector2.zero;
            currentRigidBody2D.angularVelocity = 0;
            currentAnimator.SetTrigger("Explosion");
        }
    }

    public void DestroyCannonBall()
    {
        if(cannonBallFallingController)
            cannonBallFallingController.CannonBallDestroyedEventCallback();
        gameObject.SetActive(false);
    }
}
