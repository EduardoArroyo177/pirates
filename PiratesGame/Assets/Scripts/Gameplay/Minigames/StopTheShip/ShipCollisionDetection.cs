﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class ShipCollisionDetection : MonoBehaviour
{
    [Header("Tag names")]
    [SerializeField]
    private string cannonBallTagName;
    [SerializeField]
    private string cannonBallActivatorTagName;
    [SerializeField]
    private string endLevelTagName;

    [Header("Events")]
    [SerializeField]
    private BoolEvent stopMinigameEvent;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Activate 
        if (collision.CompareTag(cannonBallActivatorTagName))
        {
            collision.gameObject.GetComponent<CannonBallFallingController>().ShootBall();
        }
        // Game over
        else if (collision.CompareTag(cannonBallTagName))
        {
            // Play dead animation and stop ship
            GetComponent<ShipMovement>().CanMove = false;
            animator.SetTrigger("Dead");
        }
        else if (collision.CompareTag(endLevelTagName))
        {
            Debug.Log("Won");
            stopMinigameEvent.Raise(true);
        }
    }

    public void StopMinigame()
    {
        stopMinigameEvent.Raise(false);
    }
}
