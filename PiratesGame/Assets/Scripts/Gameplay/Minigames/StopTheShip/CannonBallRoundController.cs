﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CannonBallRoundController : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField]
    private float[] gravityPerRound;
    [SerializeField]
    private float[] massPerRound;

    public IntEvent CurrentRoundEvent { get; set; }

    private Rigidbody2D currentRigidBody;

    public void Initialize()
    {
        currentRigidBody = GetComponent<Rigidbody2D>();
        CurrentRoundEvent.Register(CurrentRoundEventCallback);
    }

    private void CurrentRoundEventCallback(int _currentRound)
    {
        // Set values
        currentRigidBody.gravityScale = gravityPerRound[_currentRound];
        currentRigidBody.mass = massPerRound[_currentRound];
    }

    public void UnregisterEvents()
    {
        CurrentRoundEvent.Unregister(CurrentRoundEventCallback);
    }
}
