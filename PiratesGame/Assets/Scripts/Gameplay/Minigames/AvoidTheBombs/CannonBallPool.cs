﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class CannonBallPool : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private float[] movementSpeedPerRound;
    [SerializeField]
    private float ballSpawnRate;
    [SerializeField]
    private float[] ballSpawnRatePerRound;

    [Header("Cannon balls")]
    [SerializeField]
    private GameObject[] cannonBalls;

    [Header("Limits")]
    [SerializeField]
    private Transform rightLimit;
    [SerializeField]
    private Transform leftLimit;

    [Header("Direction")]
    [SerializeField]
    private bool isRight;

    public IntEvent CurrentRoundEvent { get; set; }

    public void Initialze()
    {
        CurrentRoundEvent.Register(CurrentRoundEventCallback);
    }

    private void CurrentRoundEventCallback(int _currentRound)
    {
        movementSpeed = movementSpeedPerRound[_currentRound];
        ballSpawnRate = ballSpawnRatePerRound[_currentRound];
    }

    private void Start()
    {
        StartCoroutine("SpawnBalls");
    }

    void Update()
    {
        if (isRight)
        {
            transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
        }

        if (transform.position.x >= rightLimit.position.x)
        {
            isRight = false;
        }
        else if (transform.position.x <= leftLimit.position.x)
        {
            isRight = true;
        }
    }

    private GameObject GetCannonBall()
    {
        for (int i = 0; i < cannonBalls.Length; i++)
        {
            if (!cannonBalls[i].activeSelf)
            {
                return cannonBalls[i];
            }
        }
        return null;
    }

    private IEnumerator SpawnBalls()
    {
        while (true)
        {
            yield return new WaitForSeconds(ballSpawnRate);
            GameObject spawnedBall = GetCannonBall();
            spawnedBall.transform.position = transform.position;
            spawnedBall.SetActive(true);
            spawnedBall.GetComponent<CannonBallFallingController>().ShootBall();
        }
    }

    public void UnregisterEvents()
    {
        CurrentRoundEvent.Unregister(CurrentRoundEventCallback);
    }
}
