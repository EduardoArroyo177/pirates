﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CharacterCollisionDetection : MonoBehaviour
{

    [Header("Events")]
    [SerializeField]
    private BoolEvent stopMinigameEvent;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Game over
        if (collision.CompareTag("CannonBall") || collision.CompareTag("Bottom") || collision.CompareTag("Enemy"))
        {
            // Play animation and stop movement
            CharacterMovement_AvoidStuff characterMovement = GetComponent<CharacterMovement_AvoidStuff>();
            if (characterMovement)
            {
                characterMovement.CanMove = false;
            }
            animator.SetTrigger("Dead");
            //stopMinigameEvent.Raise(false);
        }
        else if (collision.CompareTag("Skull"))
        {
            stopMinigameEvent.Raise(true);
        }
    }

    public void StopMinigame()
    {
        stopMinigameEvent.Raise(false);
    }
}
