﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class AvoidTheBombsBootstrapper : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField]
    private MinigameRoundValuesController roundValuesController;
    [SerializeField]
    private CharacterMovement_AvoidStuff characterMovement;
    [SerializeField]
    private CannonBallPool[] cannonBallPool;
    [SerializeField]
    private CannonBallRoundController[] cannonBallRoundController;
    [SerializeField]
    private CannonBallFallingController[] cannonBallFallingController;

    [Header("Events")]
    [SerializeField]
    private IntEvent currentRoundEvent;

    private void Start()
    {
        characterMovement.CurrentRoundEvent = currentRoundEvent;
        characterMovement.Initialze();

        for (int i = 0; i < cannonBallPool.Length; i++)
        {
            cannonBallPool[i].CurrentRoundEvent = currentRoundEvent;
            cannonBallPool[i].Initialze();
        }

        for (int i = 0; i < cannonBallRoundController.Length; i++)
        {
            cannonBallRoundController[i].CurrentRoundEvent = currentRoundEvent;
            cannonBallRoundController[i].Initialize();
        }

        for (int i = 0; i < cannonBallFallingController.Length; i++)
        {
            cannonBallFallingController[i].CurrentRoundEvent = currentRoundEvent;
            cannonBallFallingController[i].Initialze();
        }

        roundValuesController.CurrentRoundEvent = currentRoundEvent;
        roundValuesController.Initialize();
    }

    private void OnDestroy()
    {
        characterMovement.UnregisterEvents();

        for (int i = 0; i < cannonBallPool.Length; i++)
        {
            cannonBallPool[i].UnregisterEvents();
        }

        for (int i = 0; i < cannonBallRoundController.Length; i++)
        {
            cannonBallRoundController[i].UnregisterEvents();
        }

        for (int i = 0; i < cannonBallFallingController.Length; i++)
        {
            cannonBallFallingController[i].UnregisterEvents();
        }
    }
}
