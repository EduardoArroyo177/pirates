﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CharacterMovement_AvoidStuff : MonoBehaviour
{

    [Header("Variables")]
    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private float[] movementSpeedPerRound;
    [SerializeField]
    private bool isFacingRight;

    [Header("Events")]
    [SerializeField]
    private VoidEvent leftTapEvent;
    [SerializeField]
    private VoidEvent rightTapEvent;
    [SerializeField]
    private VoidEvent releaseTapEvent;

    public IntEvent CurrentRoundEvent { get; set; }
    public bool CanMove { get => canMove; set => canMove = value; }

    private SpriteRenderer characterSpriteRenderer;
    private Animator characterAnimator;

    private bool canMove;
    private bool isLeft;

    public void Initialze()
    {
        CurrentRoundEvent.Register(CurrentRoundEventCallback);
    }

    private void CurrentRoundEventCallback(int _currentRound)
    {
        movementSpeed = movementSpeedPerRound[_currentRound];
    }

    void OnEnable()
    {
        characterSpriteRenderer = GetComponent<SpriteRenderer>();
        characterAnimator = GetComponent<Animator>();
        leftTapEvent.Register(LeftMovementEventCallback);
        rightTapEvent.Register(RightMovementEventCallback);
        releaseTapEvent.Register(StopShipEventCallback);
    }

    void Update()
    {
        if (CanMove)
        {
            if (isLeft)
            {
                transform.Translate(Vector2.left * movementSpeed * Time.deltaTime);
                if(isFacingRight)
                    characterSpriteRenderer.flipX = true;
                else
                    characterSpriteRenderer.flipX = false;
            }
            else
            {
                transform.Translate(Vector2.right * movementSpeed * Time.deltaTime);
                if(isFacingRight)
                    characterSpriteRenderer.flipX = false;
                else
                    characterSpriteRenderer.flipX = true;
            }
        }
    }

    private void StopShipEventCallback()
    {
        if (!characterAnimator)
            characterAnimator = GetComponent<Animator>();
        characterAnimator.SetTrigger("Idle");
        CanMove = false;
    }

    private void LeftMovementEventCallback()
    {
        characterAnimator.SetTrigger("Run");
        CanMove = true;
        isLeft = true;
    }

    private void RightMovementEventCallback()
    {
        characterAnimator.SetTrigger("Run");
        CanMove = true;
        isLeft = false;
    }

    public void UnregisterEvents()
    {
        leftTapEvent.Unregister(LeftMovementEventCallback);
        rightTapEvent.Unregister(RightMovementEventCallback);
        releaseTapEvent.Unregister(StopShipEventCallback);
        CurrentRoundEvent.Unregister(CurrentRoundEventCallback);
    }
}
