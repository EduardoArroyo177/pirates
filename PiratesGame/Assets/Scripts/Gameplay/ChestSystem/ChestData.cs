﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Chest", menuName = "Pirates/ChestData", order = 1)]
public class ChestData : ScriptableObject
{
    [SerializeField]
    private ChestType chestType;
    [SerializeField]
    private int coins;
    [SerializeField]
    private int blueDiamonds;
    [SerializeField]
    private int greenDiamonds;
    [SerializeField]
    private int redDiamonds;

    public int Coins { get => coins; }
    public int BlueDiamonds { get => blueDiamonds; }
    public int GreenDiamonds { get => greenDiamonds; }
    public int RedDiamonds { get => redDiamonds; }
    public ChestType ChestType { get => chestType; set => chestType = value; }
}
