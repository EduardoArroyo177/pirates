﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChestType
{ 
    bronze,
    silver, 
    golden,
    platinum
}

public class ChestSystem : MonoBehaviour
{
    public static ChestSystem instance;

    [SerializeField]
    private ChestData bronzeChestData;
    [SerializeField]
    private ChestData silverChestData;
    [SerializeField]
    private ChestData goldenChestData;
    [SerializeField]
    private ChestData platinumChestData;

    // TODO: Remove serialize field as this is nly for testing
    [SerializeField]
    private bool hasAdventureMap;
    [SerializeField]
    private int currentEarnedChest;

    public bool HasAdventureMap { get => hasAdventureMap; set => hasAdventureMap = value; }
    public int CurrentEarnedChest { get => currentEarnedChest; set => currentEarnedChest = value; }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public ChestData GetEarnedChest()
    {
        switch (currentEarnedChest)
        {
            case 1:
                return bronzeChestData;
            case 2:
                return silverChestData;
            case 3:
                return goldenChestData;
            case 4:
                return platinumChestData;
            default:
                return platinumChestData;
        }
    }
}
