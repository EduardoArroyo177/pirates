﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class MinigameManager : MonoBehaviour
{
    [SerializeField]
    private BaseGameplayData island1BaseData;

    public FloatEvent TimerDataEvent { get; set; }

    public void Initialize()
    {
        TimerDataEvent.Raise(island1BaseData.RoundsPlayTime[0]);
    }

}
