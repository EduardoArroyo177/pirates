﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class GameplayBootstrapper : MonoBehaviour
{
    [Header("Generic scripts")]
    [SerializeField]
    private MinigameManager minigameManager;
    [SerializeField]
    private MinigamePool minigamePool;
    [SerializeField]
    private CoreGameplayManager coreGameplayManager;

    [Header("Gameplay Scripts")]
    [SerializeField]
    private GameplayView gameplayView;
    [SerializeField]
    private GameplayController gameplayController;
    [SerializeField]
    private GameplayModel gameplayModel;

    [Header("Mission Finished Scripts")]
    [SerializeField]
    private MissionFinishedModel missionFinishedModel;
    [SerializeField]
    private MissionFinishedController missionFinishedController;
    [SerializeField]
    private MissionFinishedView missionFinishedView;

    [Header("Game over scripts")]
    [SerializeField]
    private GameOverView gameOverView;

    [Header("Faster scripts")]
    [SerializeField]
    private FasterController fasterController;

    [Header("Events")]
    [SerializeField]
    private VoidEvent startGameEvent;
    [SerializeField]
    private VoidEvent stopGameEvent;
    [SerializeField]
    private FloatEvent timerDataEvent;
    [SerializeField]
    private StringPairEvent minigameTextDataEvent;
    [SerializeField]
    private VoidEvent startMinigameEvent;
    [SerializeField]
    private BoolEvent stopMinigameEvent;

    void Awake()
    {
        InitializeEvents();
    }

    private void InitializeEvents()
    {
        // Generic scripts
        minigameManager.TimerDataEvent = timerDataEvent;

        // Models
        gameplayModel.TimerDataEvent = timerDataEvent;
        gameplayModel.InitializeData();

        // Initialize data
        minigameManager.Initialize();

        // Controllers
        gameplayController.StartGameEvent = startGameEvent;
        gameplayController.StopGameEvent = stopGameEvent;
        gameplayController.StopMinigameEvent = stopMinigameEvent;
        gameplayController.MinigameTextDataEvent = minigameTextDataEvent;
        gameplayController.InitializeData();

        missionFinishedController.coreGameplayManager = coreGameplayManager;
        missionFinishedController.missionFinishedView = missionFinishedView;
        missionFinishedController.missionFinishedModel = missionFinishedModel;
        missionFinishedController.gameOverView = gameOverView;
        missionFinishedController.fasterController = fasterController;
        missionFinishedController.StopMinigameEvent = stopMinigameEvent;
        missionFinishedController.Initialize();

        fasterController.coreGameplayManager = coreGameplayManager;

        // Core gameplay
        coreGameplayManager.StartMinigameEvent = startMinigameEvent;
        coreGameplayManager.minigamePool = minigamePool;
        coreGameplayManager.minigameManager = minigameManager;
        coreGameplayManager.gameplayController = gameplayController;
        coreGameplayManager.missionFinishedController = missionFinishedController;
        coreGameplayManager.Initialize();
    }
}
