﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityAtoms.BaseAtoms;

public class LeftRightTapController : MonoBehaviour
{
    [Header("Events")]
    [SerializeField]
    private VoidEvent leftTapEvent;
    [SerializeField]
    private VoidEvent rightTapEvent;
    [SerializeField]
    private VoidEvent releaseTapEvent;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x > Screen.width / 2)
            {
                rightTapEvent.Raise();
            }
            else if (Input.mousePosition.x < Screen.width / 2)
            {
                leftTapEvent.Raise();
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            releaseTapEvent.Raise();
        }
    }
}
