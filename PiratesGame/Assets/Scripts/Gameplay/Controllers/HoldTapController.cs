﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityAtoms.BaseAtoms;

public class HoldTapController : MonoBehaviour
{
    [Header("Events")]
    [SerializeField]
    private VoidEvent holdTapEvent;
    [SerializeField]
    private VoidEvent releaseTapEvent;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            holdTapEvent.Raise();
        }

        if (Input.GetMouseButtonUp(0))
        {
            releaseTapEvent.Raise();
        }
    }

    void OnDestroy()
    {
        holdTapEvent.UnregisterAll();
        releaseTapEvent.UnregisterAll();
    }
}
