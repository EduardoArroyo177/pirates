﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class NormalTapController : MonoBehaviour
{
    [Header("Events")]
    [SerializeField]
    private VoidEvent tapEvent;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            tapEvent.Raise();
        }
    }
}
