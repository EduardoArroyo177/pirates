﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityAtoms.BaseAtoms;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverView : MonoBehaviour
{
    [Header("Script references")]
    [SerializeField]
    private MissionFinishedModel missionFinishedModel;

    [Header("Other references")]
    [SerializeField]
    private GameObject chestWonView;
    [SerializeField]
    private float timeToDisableAds;

    [Header("UI References")]
    [SerializeField]
    private GameObject skullIcon;
    [SerializeField]
    private GameObject heartIcon;
    [SerializeField]
    private TextMeshProUGUI totalCoinsTxt;

    [Header("Ads helpers")]
    [SerializeField]
    private Image remainingTimeBar;
    [SerializeField]
    private Button watchAdButton;

    [Header("Ads")]
    [SerializeField]
    private AdsManager adsManager;
    [SerializeField]
    private VoidEvent GiveRewardEvent;

    private float timeHelper = 0;

    // TODO: Remove at the end
    [SerializeField]
    private float remainingSeconds;

    private void Awake()
    {
        GiveRewardEvent.Register(GiveRewardEventCallback);
    }

    public void ShowGameOverView(bool _roundsFinished)
    {
        totalCoinsTxt.text = missionFinishedModel.TotalCoins.ToString();

        if (_roundsFinished)
        {
            heartIcon.SetActive(true);
        }
        else
        {
            skullIcon.SetActive(true);
        }

        gameObject.SetActive(true);

        if (ChestSystem.instance.HasAdventureMap && ChestSystem.instance.CurrentEarnedChest > 0)
        {
            chestWonView.SetActive(true);
        }
    }

    public void GetDoubleCoins()
    {
        Debug.Log("Double the coins!");
        adsManager.ShowRewardedAd();
        //GiveRewardEventCallback();
    }

    private void GiveRewardEventCallback()
    {
        SaveSystem.instance.SaveCoins(missionFinishedModel.TotalCoins);
        missionFinishedModel.TotalCoins *= 2;
        totalCoinsTxt.text = missionFinishedModel.TotalCoins.ToString();
        watchAdButton.interactable = false;
    }

    private void Update()
    {
        timeHelper += Time.deltaTime / timeToDisableAds;
        remainingSeconds = Mathf.Lerp(timeToDisableAds, 0f, timeHelper);
        remainingTimeBar.fillAmount = remainingSeconds / timeToDisableAds;
        if (remainingSeconds.Equals(0))
        {
            // Disable button
            watchAdButton.interactable = false;
        }
    }

    public void BackToMap()
    {
        StartCoroutine("LoadMapScene");
    }

    IEnumerator LoadMapScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainMenu");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    private void OnDestroy()
    {
        GiveRewardEvent.Unregister(GiveRewardEventCallback);
    }
}
