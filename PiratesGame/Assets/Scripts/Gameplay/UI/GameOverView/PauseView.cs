﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseView : MonoBehaviour
{
    [SerializeField]
    private LoadController loadController;

    private void OnEnable()
    {
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        Time.timeScale = 1;
        loadController.LoadMainMenu();
    }
}
