﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionFinishedModel : MonoBehaviour
{
    [SerializeField]
    private int playerLives;
    [SerializeField]
    private float timeToCloseScreen;
    [SerializeField]
    private int totalCoins;

    public int PlayerLives { get => playerLives; set => playerLives = value; }
    public float TimeToCloseScreen { get => timeToCloseScreen; set => timeToCloseScreen = value; }

    public int TotalCoins { get => totalCoins; set => totalCoins = value; }
}
