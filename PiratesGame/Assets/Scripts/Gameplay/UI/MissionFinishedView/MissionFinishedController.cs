﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class MissionFinishedController : MonoBehaviour
{
    // Scripts
    public MissionFinishedView missionFinishedView { get; set; }
    public MissionFinishedModel missionFinishedModel { get; set; }
    public CoreGameplayManager coreGameplayManager { get; set; }
    public GameOverView gameOverView { get; set; }
    public FasterController fasterController { get; set; }

    // Events
    public BoolEvent StopMinigameEvent { get; set; }


    public void Initialize()
    {
        StopMinigameEvent.Register(MinigameFinished);
    }

    private void MinigameFinished(bool _gameWon)
    {
        if (_gameWon)
        {
            missionFinishedView.ShowMinigameFinishedView(missionFinishedModel.PlayerLives);
            missionFinishedView.SetCoinsEarned(coreGameplayManager.currentMinigame.CoinsToWin);
            missionFinishedView.SetTotalCoins(missionFinishedModel.TotalCoins, coreGameplayManager.currentMinigame.CoinsToWin);
            missionFinishedModel.TotalCoins += coreGameplayManager.currentMinigame.CoinsToWin;
            SaveSystem.instance.SaveCoins(coreGameplayManager.currentMinigame.CoinsToWin);
        }
        else
        {
            missionFinishedModel.PlayerLives--;
            missionFinishedView.SetCoinsEarned(0);
            missionFinishedView.ShowMinigameFinishedView(missionFinishedModel.PlayerLives);
        }

        coreGameplayManager.PrepareNextMinigame();
        ScreenShownCounter();
    }

    public void GameFinished()
    {
        coreGameplayManager.GameOver();
        gameOverView.ShowGameOverView(true);
    }

    private void ScreenShownCounter()
    {
        StartCoroutine("StartNewMinigame");
    }

    private IEnumerator StartNewMinigame()
    {
        
        yield return new WaitForSeconds(missionFinishedModel.TimeToCloseScreen);

        if (missionFinishedModel.PlayerLives <= 0)
        {
            gameOverView.ShowGameOverView(false);
            StopCoroutine("StartNewMinigame");
        }
        else
        {

            if (coreGameplayManager.ShowFasterView)
            {
                // Show faster view
                coreGameplayManager.ShowFasterView = false;
                fasterController.ShowFasterView();
            }
            else
            {
                // Raise event for new mini game
                coreGameplayManager.StartNextMinigame();
            }
        }
        // Then we close the screen
        missionFinishedView.CloseScreen();
    }

    private void OnDestroy()
    {
    }
}
