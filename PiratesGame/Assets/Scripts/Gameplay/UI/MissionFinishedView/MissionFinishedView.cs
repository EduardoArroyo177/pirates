﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Runtime.InteropServices;

public class MissionFinishedView : MonoBehaviour
{
    [SerializeField]
    private AudioListener mainCameraAudioListener;

    [Header("UI References")]
    [SerializeField]
    private GameObject[] hearthIcons;
    [SerializeField]
    private TextMeshProUGUI totalCoinsTxt;
    [SerializeField]
    private TextMeshProUGUI earnedCoinsTxt;

    private void OnEnable()
    {
        mainCameraAudioListener.enabled = true;
    }

    private void OnDisable()
    {
        mainCameraAudioListener.enabled = false;

        for (int i = 0; i < hearthIcons.Length; i++)
        {
            hearthIcons[i].SetActive(false);
        }
    }

    public void ShowMinigameFinishedView(int _remainigHearts)
    {
        for (int i = 0; i < hearthIcons.Length; i++)
        {
            hearthIcons[i].SetActive(true);
        }
        gameObject.SetActive(true);
        switch (_remainigHearts)
        {
            case 4: // TODO: Once IAP Are working
                break;
            case 3:
                break;
            case 2:
                hearthIcons[2].GetComponent<Animator>().SetTrigger("HeartLost");
                break;
            case 1:
                hearthIcons[2].SetActive(false);
                hearthIcons[1].GetComponent<Animator>().SetTrigger("HeartLost");
                break;
            case 0:
                hearthIcons[2].SetActive(false);
                hearthIcons[1].SetActive(false);
                hearthIcons[0].GetComponent<Animator>().SetTrigger("HeartLost");
                break;

        }
        
    }

    public void SetCoinsEarned(int _coins)
    {
        earnedCoinsTxt.text = _coins.ToString();
    }

    public void SetTotalCoins(int _currentCoins, int _newCoins)
    {
        StartCoroutine(CountAnimation(_currentCoins, _newCoins));
    }

    IEnumerator CountAnimation(int _currentCoins, int _earnedCoins)
    {
        int coinCounter = _currentCoins;
        int totalCoins = _currentCoins + _earnedCoins;
        while (coinCounter < totalCoins)
        {
            yield return null;
            coinCounter++;
            totalCoinsTxt.text = $"{coinCounter}";
        }
    }

    public void CloseScreen()
    {
        gameObject.SetActive(false);
    }
}
