﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    [SerializeField]
    private GameplayModel gameplayModel;
    [SerializeField]
    private GameplayView gameplayView;
    [SerializeField]
    private bool timerFinishedWins;

    
    public VoidEvent StartGameEvent { get; set; }
    public VoidEvent StopGameEvent { get; set; }
    public BoolEvent StopMinigameEvent { get; set; }
    public StringPairEvent MinigameTextDataEvent { get; set; }


    // TODO: Remove at the end
    [SerializeField]
    private float remainingSeconds;

    private float minigameTime;
    private bool timerStarted;

    private float timeHelper;

    public void InitializeData()
    {
        StartGameEvent.Register(StartGame);
        StopMinigameEvent.Register(MiniGameFinished);
        StopGameEvent.Register(StopGame);
        MinigameTextDataEvent.Register(SetMinigameData);

        minigameTime = gameplayModel.MinigameTime;
    }

    public void SetMinigameData(StringPair _minigameData)
    {
        gameplayView.SetData(_minigameData);
    }

    public void SetTimerFinishedBehaviorData(bool _timerFinishedWins)
    {
        timerFinishedWins = _timerFinishedWins;
    }

    private void StartGame()
    {
        timeHelper = 0;
        timerStarted = true;
    }

    private void MiniGameFinished(bool _gameWon)
    {
        StopCoroutine("StartTimer");
        timerStarted = false;
    }

    private void StopGame()
    {
        StopCoroutine("StartTimer");
    }
    
    private void Update()
    {
        if (timerStarted)
        {
            timeHelper += Time.deltaTime / minigameTime;
            remainingSeconds = Mathf.Lerp(minigameTime, 0f, timeHelper);
            gameplayView.DecreaseTime(remainingSeconds / minigameTime);
            if (remainingSeconds.Equals(0))
            {
                timerStarted = false;
                // Finish level
                if (timerFinishedWins)
                {
                    StopMinigameEvent.Raise(true);
                }
                else
                {
                    StopMinigameEvent.Raise(false);
                }
            }
        }
    }

    private void OnDestroy()
    {
        StartGameEvent.Unregister(StartGame);
        StopMinigameEvent.Unregister(MiniGameFinished);
        StopGameEvent.Unregister(StopGame);
        MinigameTextDataEvent.Unregister(SetMinigameData);
    }
}
