﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayView : MonoBehaviour
{
    [SerializeField]
    private Image remainingTimeBar;
    [SerializeField]
    private TextMeshProUGUI minigameNameTxt;
    [SerializeField]
    private TextMeshProUGUI minigameInstructionsTxt;
    [SerializeField]
    private float timeToDisappearTexts;

    public void SetData(StringPair _minigameData)
    {
        minigameNameTxt.gameObject.SetActive(true);
        minigameInstructionsTxt.gameObject.SetActive(true);
        minigameNameTxt.text = _minigameData.Item1;
        minigameInstructionsTxt.text = _minigameData.Item2;
        //StartCoroutine("TurnTextOff");
    }

    public void DecreaseTime(float value)
    {
        remainingTimeBar.fillAmount = value;
    }

    IEnumerator TurnTextOff()
    {
        yield return new WaitForSeconds(timeToDisappearTexts);
        minigameNameTxt.gameObject.SetActive(false);
        minigameInstructionsTxt.gameObject.SetActive(false);
    }
}
