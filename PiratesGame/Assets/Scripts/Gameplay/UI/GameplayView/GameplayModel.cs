﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms.BaseAtoms;
using UnityEngine;

public class GameplayModel : MonoBehaviour
{

    [SerializeField]
    private float minigameTime;

    public float MinigameTime { get => minigameTime; set => minigameTime = value; }

    public FloatEvent TimerDataEvent { get; set; }

    public void InitializeData()
    {
        TimerDataEvent.Register(SetTimerData);
    }

    private void SetTimerData(float _timerData)
    {
        MinigameTime = _timerData;
    }

    private void OnDestroy()
    {
        TimerDataEvent.Unregister(SetTimerData);
    }
}
