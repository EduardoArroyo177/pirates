﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestAnimationController : MonoBehaviour
{
    [SerializeField]
    private ChestWonView chestWonView;

    public void ChestAnimationFinished()
    {
        chestWonView.OpenRewardsView();
    }
}
