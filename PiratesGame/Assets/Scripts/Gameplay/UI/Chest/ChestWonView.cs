﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChestWonView : MonoBehaviour
{
    [SerializeField]
    private Animator chestAnimator;
    [SerializeField]
    private GameObject descriptionTxt;
    [SerializeField]
    private GameObject rewardView;

    [Header("UI References")]
    [SerializeField]
    private TextMeshProUGUI titleTxt;
    [SerializeField]
    private TextMeshProUGUI coinsTxt;
    [SerializeField]
    private TextMeshProUGUI blueDiamondsTxt;
    [SerializeField]
    private TextMeshProUGUI greenDiamondsTxt;
    [SerializeField]
    private TextMeshProUGUI redDiamondsTxt;

    private void Awake()
    {
        LoadData();
    }

    private void LoadData()
    {
        ChestData earnedChest = ChestSystem.instance.GetEarnedChest();
        titleTxt.text = $"You found a {earnedChest.ChestType} chest!";
        coinsTxt.text = earnedChest.Coins.ToString();
        blueDiamondsTxt.text = earnedChest.BlueDiamonds.ToString();
        greenDiamondsTxt.text = earnedChest.GreenDiamonds.ToString();
        redDiamondsTxt.text = earnedChest.RedDiamonds.ToString();

        SaveSystem.instance.SaveCoins(earnedChest.Coins);
        SaveSystem.instance.SaveDiamonds(earnedChest.BlueDiamonds, earnedChest.GreenDiamonds, earnedChest.RedDiamonds);
    }

    public void PlayChestAnimation()
    {
        chestAnimator.SetTrigger("OpenChest");
    }

    public void OpenRewardsView()
    {
        descriptionTxt.SetActive(false);
        rewardView.SetActive(true);
    }
}
