﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FasterController : MonoBehaviour
{
    // Variables
    [SerializeField]
    private GameObject fasterParent;

    // Script references
    public CoreGameplayManager coreGameplayManager { get; set; }

    public void ShowFasterView()
    {
        fasterParent.SetActive(true);
    }

    public void StartNextRound()
    {
        // Start minigame
        coreGameplayManager.StartNextMinigame();
        fasterParent.SetActive(false);
    }
}
