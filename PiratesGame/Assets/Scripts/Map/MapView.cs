﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MapView : MonoBehaviour
{
    [Header("Script references")]
    [SerializeField]
    private UseMapView useMapView;
    [SerializeField]
    private LoadController loadController;
    [SerializeField]
    private IslandBlockedView islandBlockedView;

    [Header("UI References")]
    [SerializeField]
    private Image lastUsedCharacterIcon;
    [SerializeField]
    private TextMeshProUGUI playerLevelTxt;
    [SerializeField]
    private TextMeshProUGUI playerCoinsTxt;
    [SerializeField]
    private TextMeshProUGUI playerMapsTxt;

    private void Start()
    {
        UnlockIslands();
    }

    private void UnlockIslands()
    {
        PlayerStats playerStats = SaveSystem.instance.GetPlayerData();
        if (!playerStats.island2Unlocked)
        {
            if (playerStats.blueDiamonds >= islandBlockedView.IslandCost[1].blueDiamonds
                && playerStats.greenDiamonds >= islandBlockedView.IslandCost[1].greenDiamonds
                && playerStats.redDiamonds >= islandBlockedView.IslandCost[1].redDiamonds)
            {
                SaveSystem.instance.UnlockIsland(1);
            }
        }

        if (!playerStats.island3Unlocked)
        {
            if (playerStats.blueDiamonds >= islandBlockedView.IslandCost[2].blueDiamonds
                && playerStats.greenDiamonds >= islandBlockedView.IslandCost[2].greenDiamonds
                && playerStats.redDiamonds >= islandBlockedView.IslandCost[2].redDiamonds)
            {
                SaveSystem.instance.UnlockIsland(2);
            }
        }

        if (!playerStats.island4Unlocked)
        {
            if (playerStats.blueDiamonds >= islandBlockedView.IslandCost[3].blueDiamonds
                && playerStats.greenDiamonds >= islandBlockedView.IslandCost[3].greenDiamonds
                && playerStats.redDiamonds >= islandBlockedView.IslandCost[3].redDiamonds)
            {
                SaveSystem.instance.UnlockIsland(3);
            }
        }

        if (!playerStats.island5Unlocked)
        {
            if (playerStats.blueDiamonds >= islandBlockedView.IslandCost[4].blueDiamonds
                && playerStats.greenDiamonds >= islandBlockedView.IslandCost[4].greenDiamonds
                && playerStats.redDiamonds >= islandBlockedView.IslandCost[4].redDiamonds)
            {
                SaveSystem.instance.UnlockIsland(4);
            }
        }

        if (!playerStats.island6Unlocked)
        {
            if (playerStats.blueDiamonds >= islandBlockedView.IslandCost[5].blueDiamonds
                && playerStats.greenDiamonds >= islandBlockedView.IslandCost[5].greenDiamonds
                && playerStats.redDiamonds >= islandBlockedView.IslandCost[5].redDiamonds)
            {
                SaveSystem.instance.UnlockIsland(5);
            }
        }
    }

    public void ShowPlayerStats(PlayerStats _playerStats)
    { 
        playerCoinsTxt.text = $"x {_playerStats.totalCoins.ToString()}";
        playerMapsTxt.text = $"x {_playerStats.maps.ToString()}";
    }

    public void CheckIfHasMap(int _selectedIsland)
    {
        PlayerStats playerStats = SaveSystem.instance.GetPlayerData();

        // First check if island is unlocked pls
        switch (_selectedIsland)
        {
            case 1: // Island 2
                if (!playerStats.island2Unlocked)
                {
                    islandBlockedView.ShowIslandCost(_selectedIsland);
                    return;
                }
                break;
            case 2:
                if (!playerStats.island3Unlocked)
                {
                    islandBlockedView.ShowIslandCost(_selectedIsland);
                    return;
                }
                break;
            case 3:
                if (!playerStats.island4Unlocked)
                {
                    islandBlockedView.ShowIslandCost(_selectedIsland);
                    return;
                }
                break;
            case 4:
                if (!playerStats.island5Unlocked)
                {
                    islandBlockedView.ShowIslandCost(_selectedIsland);
                    return;
                }
                break;
            case 5:
                if (!playerStats.island6Unlocked)
                {
                    islandBlockedView.ShowIslandCost(_selectedIsland);
                    return;
                }
                break;
        }

        if (playerStats.maps > 0)
        {
            useMapView.SelectedIsland = _selectedIsland;
            useMapView.gameObject.SetActive(true);
        }
        else
        {
            loadController.LoadIslandScene(_selectedIsland);
        }
    }
}
