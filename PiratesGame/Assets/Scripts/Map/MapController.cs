﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    [SerializeField]
    private MapModel mapModel;
    [SerializeField]
    private MapView mapView;

    private PlayerStats playerStats;

    private void Start()
    {
        LoadPlayerStats();
    }

    private void LoadPlayerStats()
    {
        playerStats = SaveSystem.instance.GetPlayerData();
        mapView.ShowPlayerStats(SaveSystem.instance.GetPlayerData());
    }

    //private void Start()
    //{
    //    SendViewData();
    //}

    //private void SendViewData()
    //{
    //    PlayerData playerData = new PlayerData();
    //    playerData.playerLevel = Inventory.instance.PlayerLevel;
    //    playerData.lastUsedCharacter = Inventory.instance.LastUsedCharacter;
    //    playerData.playerCoins = Inventory.instance.PlayerCoins;
    //    playerData.playerMaps = Inventory.instance.PlayerMaps;

    //    int index = mapModel.AvailableCharacters.FindIndex(x => x.character.Equals(playerData.lastUsedCharacter));

    //    if (index >= 0)
    //    {
    //        playerData.lastUsedCharacterSprite = mapModel.AvailableCharacters[index].characterSprite;
    //    }

    //    string playerDataJson = JsonUtility.ToJson(playerData);

    //    mapView.SetPlayerData(playerDataJson);
    //}

    public void SetLevelData()
    { 
    
    }
}
